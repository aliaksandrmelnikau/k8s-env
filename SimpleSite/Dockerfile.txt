#FROM ubuntu:20.04
FROM python:3.9.0a6-alpine

#RUN apt update -y
#RUN apt install python3 -y
#RUN apt install -y python3-pip
RUN python3 -m pip install Django

#ENV MY_NODE_NAME=test
#ENV MY_POD_NAME=test
#ENV MY_POD_NAMESPACE=test
#ENV MY_POD_IP=test
#ENV MY_POD_SERVICE_ACCOUNT=test

COPY ./SimpleSite /app

WORKDIR /app

EXPOSE 80

CMD ["python3", "manage.py", "runserver", "0.0.0.0:80"]